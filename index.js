function tinhDiemKhuVuc(khuVuc) {
  switch (khuVuc) {
    case "x":
      return 0;
    case "a":
      return 2;
    case "b":
      return 1;
    case "c":
      return 0.5;
  }
}
function tinhDiemDoiTuong(doiTuong) {
  switch (doiTuong) {
    case "0":
      return 0;
    case "1":
      return 2.5;
    case "2":
      return 1.5;
    case "3":
      return 1;
  }
}

function tinhDiem() {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var khuVuc = document.getElementById("txt-khu-vuc").value;
  var diemKhuVuc = tinhDiemKhuVuc(khuVuc);
  var doiTuong = document.getElementById("txt-doi-tuong").value;
  var diemDoiTuong = tinhDiemDoiTuong(doiTuong);
  var diemMon1 = document.getElementById("txt-diem-mon-thu-1").value * 1;
  var diemMon2 = document.getElementById("txt-diem-mon-thu-2").value * 1;
  var diemMon3 = document.getElementById("txt-diem-mon-thu-3").value * 1;
  var tongDiem = diemMon1 + diemMon2 + diemMon3;
  if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
    document.getElementById(
      "resultEx1"
    ).innerHTML = `<p>Kết quả: Bạn đã rớt tốt nghiệp</p>`;
  } else {
    var diemTongKet = tongDiem + diemKhuVuc + diemDoiTuong;
    if (diemTongKet >= diemChuan)
      document.getElementById(
        "resultEx1"
      ).innerHTML = `<p>Kết quả: Bạn đã đậu tốt nghiệp với tổng điểm ${diemTongKet}</p>`;
    else
      document.getElementById(
        "resultEx1"
      ).innerHTML = `<p>Kết quả: Bạn đã rớt tốt nghiệp với tổng điểm ${diemTongKet}</p>`;
  }
}
// function tinhTienDienTheoTungKw(soKwDien) {
//   if (soKwDien <= 50) {
//     return 500;
//   } else if (soKwDien > 50 && soKwDien <= 100) {
//     return 650;
//   } else if (soKwDien > 100 && soKwDien <= 200) {
//     return 850;
//   } else if (soKwDien > 200 && soKwDien <= 350) {
//     return 1100;
//   } else {
//     return 1300;
//   }
// }
function tinhTienDien() {
  var tienDienMuc1 = 500;
  var tienDienMuc2 = 650;
  var tienDienMuc3 = 850;
  var tienDienMuc4 = 1100;
  var tienDienMuc5 = 1300;
  var soKwDien = document.getElementById("txt-so-kw").value * 1;
  var tongTien;
  if (soKwDien <= 50) {
    tongTien = tienDienMuc1 * soKwDien;
  } else if (soKwDien > 50 && soKwDien <= 100) {
    tongTien = 50 * tienDienMuc1 + (soKwDien - 50) * tienDienMuc2;
  } else if (soKwDien > 100 && soKwDien <= 200) {
    tongTien =
      tienDienMuc1 * soKwDien +
      50 * tienDienMuc2 +
      (soKwDien - 100) * tienDienMuc3;
  } else if (soKwDien > 200 && soKwDien <= 350) {
    tongTien =
      tienDienMuc1 * soKwDien +
      50 * tienDienMuc2 +
      100 * tienDienMuc3 +
      (soKwDien - 200) * tienDienMuc4;
  } else {
    tongTien =
      tienDienMuc1 * soKwDien +
      50 * tienDienMuc2 +
      100 * tienDienMuc3 +
      150 * tienDienMuc4 +
      (soKwDien - 350) * tienDienMuc5;
  }
  document.getElementById(
    "resultEx2"
  ).innerHTML = `<p>Kết quả: ${tongTien} VNĐ</p>`;
}
function tinhTienThueTheoThuNhap(thuNhapChiuThue) {
  if (thuNhapChiuThue <= 60e6) {
    return 0.05;
  } else if (thuNhapChiuThue > 60e6 && thuNhapChiuThue <= 120e6) {
    return 0.1;
  } else if (thuNhapChiuThue > 120e6 && thuNhapChiuThue <= 210e6) {
    return 0.1;
  } else if (thuNhapChiuThue > 210e6 && thuNhapChiuThue <= 384e6) {
    return 0.2;
  } else if (thuNhapChiuThue > 384e6 && thuNhapChiuThue <= 624e6) {
    return 0.25;
  } else if (thuNhapChiuThue > 624e6 && thuNhapChiuThue <= 960e6) {
    return 0.3;
  } else {
    return 0.35;
  }
}
function tinhThue() {
  var tongThuNhap = document.getElementById("txt-tong-thu-nhap").value * 1;
  var soNguoiPhuThuoc =
    document.getElementById("txt-so-nguoi-phu-thuoc").value * 1;
  var hoTen = document.getElementById("txt-ho-ten").value;
  var thuNhapChiuThue = tongThuNhap - 4e6 - soNguoiPhuThuoc * 1600000;
  var mucThue = tinhTienThueTheoThuNhap(thuNhapChiuThue);
  var tienThue = thuNhapChiuThue * mucThue;
  if (tienThue <= 0) {
    document.getElementById(
      "resultEx3"
    ).innerHTML = `<p>Tiền thuế của ${hoTen} là 0 VNĐ</p>`;
  } else
    document.getElementById(
      "resultEx3"
    ).innerHTML = `<p>Tiền thuế của ${hoTen} là: ${tienThue} VNĐ</p>`;
}

function tinhTienCap() {
  var loaiKhachHang = document.getElementById("txt-loai-khach-hang").value;
  console.log("loaiKhachHang: ", loaiKhachHang);
}
function myFunction() {
  var x = document.getElementById("txt-loai-khach-hang").value;
  if (x == "doanhNghiep") {
    document.getElementById("txt-so-ket-noi").style.display = "block";
  } else {
    document.getElementById("txt-so-ket-noi").style.display = "none";
  }
}
function tinhPhiXuLyHoaDon(loaiKhachHang) {
  switch (loaiKhachHang) {
    case "nhaDan":
      return 4.5;
    case "doanhNghiep":
      return 15;
  }
}
function tinhPhiDichVuCoBan(loaiKhachHang) {
  if (loaiKhachHang == "nhaDan") {
    return 20.5;
  } else {
    return 75;
  }
}
function tinhPhiThueKenhCaoCap(loaiKhachHang) {
  if (loaiKhachHang == "nhaDan") {
    return 7.5;
  } else {
    return 50;
  }
}
function tinhTienCap() {
  var loaiKhachHang = document.getElementById("txt-loai-khach-hang").value;
  var soKenhCaoCapValue = document.getElementById("txt-kenh-cao-cap").value * 1;
  var maKhachHang = document.getElementById("txt-ma-khach-hang").value;
  var soKetNoiValue = document.getElementById("txt-so-ket-noi").value * 1;
  var phiXuLyHoaDon = tinhPhiXuLyHoaDon(loaiKhachHang);
  var phiDichVuCoBan = tinhPhiDichVuCoBan(loaiKhachHang);
  var phiThueKenhCaoCap = tinhPhiThueKenhCaoCap(loaiKhachHang);
  var tongChiPhi = 0;
  if (loaiKhachHang == "nhaDan") {
    tongChiPhi =
      phiXuLyHoaDon + phiDichVuCoBan + phiThueKenhCaoCap * soKenhCaoCapValue;
  } else if (loaiKhachHang == "doanhNghiep" && soKetNoiValue > 10) {
    tongChiPhi =
      phiXuLyHoaDon +
      phiDichVuCoBan +
      soKenhCaoCapValue * phiThueKenhCaoCap +
      (soKetNoiValue - 10) * 5;
  } else {
    tongChiPhi =
      phiXuLyHoaDon + phiDichVuCoBan + soKenhCaoCapValue * phiThueKenhCaoCap;
  }
  document.getElementById(
    "resultEx4"
  ).innerHTML = `<p>Mã khách hàng: ${maKhachHang}; Tổng tiền cáp: ${tongChiPhi}</p>`;
}
